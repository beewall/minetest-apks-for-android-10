# Android 10-Compatible Minetest Builds

# NOTE: THIS IS NO LONGER REQUIRED, AS OF THE LATEST NIGHTLIES, AND THUS WILL NOT BE BUILDING ANY MORE

Takes nightly builds of Minetest APKs and adds a few bits to their manifest so
that they will work on Android 10 (the new Scoped Storage breaks Minetest).

The APKs here are built using a method meant for testing apps, and it is not a
long-term solution (it will cease to work on Android 11, and apps using it can't
be uploaded to the Play Store). These APKs are a temporary solution to allow
people to play Minetest untill the app is able to be updated to properly support
Android 10.

## Download

### Latest
Go [here][1] to automatically download the latest APK.

### Older Versions
1. Go to [Jobs][2].
2. Find the job for the day you'd like to download it from.
3. Click the download arrow at the right.
4. Unzip the APK.

## Credits

The [.gitlab-ci.yml][3] file is licensed under the [MIT License][4]. (This is
the only thing here that's my work.)

The builds are actually made by [tsao-chi][5] for their Unstable World game
[here][6]. I just modify the APKs to work on Android 10.

And of course full credit for the app to [the Minetest devs][7] (the copyright
is mainly held by [celeron55][8], according to their LICENSE file), I didn't
make any of it myself. [Minetest][9] is licensed under the [LGPL v2.1 or later][10].

[1]:  https://gitlab.com/beewall/minetest-apks-for-android-10/builds/artifacts/master/raw/minetest.apk?job=modify%20apk
[2]:  https://gitlab.com/beewall/minetest-apks-for-android-10/-/jobs
[3]:  .gitlab-ci.yml
[4]:  https://mit-license.org/
[5]:  https://github.com/tsao-chi
[6]:  https://github.com/The-Unstable-World/binary/blob/master/client/android/minetest.apk
[7]:  https://github.com/minetest/minetest/graphs/contributors
[8]:  https://github.com/celeron55
[9]:  https://github.com/minetest/minetest
[10]: https://www.gnu.org/licenses/old-licenses/lgpl-2.1.en.html
